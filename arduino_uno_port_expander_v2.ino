#include "Arduino.h"
#include "PCF8574.h"

PCF8574 out_pcf8574(0x20);
PCF8574 in_pcf8574(0x21);

volatile boolean trigger_on = false;
unsigned long prev_millis = millis();
PCF8574::DigitalInput inputs;

void setup()
{
  Serial.begin(9600);

  Serial.println(F("Setting all pins on PCF8574 with HW address 0x20 as outputs."));
  out_pcf8574.pinMode(P0, OUTPUT);
  out_pcf8574.pinMode(P1, OUTPUT);
  out_pcf8574.pinMode(P2, OUTPUT);
  out_pcf8574.pinMode(P3, OUTPUT);
  out_pcf8574.pinMode(P4, OUTPUT);
  out_pcf8574.pinMode(P5, OUTPUT);
  out_pcf8574.pinMode(P6, OUTPUT);
  out_pcf8574.pinMode(P7, OUTPUT);

  out_pcf8574.begin();
  
  Serial.println(F("Setting all pins on PCF8574 with HW address 0x21 as inputs."));
  in_pcf8574.pinMode(P0, INPUT);
  in_pcf8574.pinMode(P1, INPUT);
  in_pcf8574.pinMode(P2, INPUT);
  in_pcf8574.pinMode(P3, INPUT);
  in_pcf8574.pinMode(P4, INPUT);
  in_pcf8574.pinMode(P5, INPUT);
  in_pcf8574.pinMode(P6, INPUT);
  in_pcf8574.pinMode(P7, INPUT);

  // A required step before we can use the pins as outputs as per the documentation
  in_pcf8574.digitalWrite(P0, HIGH);
  in_pcf8574.digitalWrite(P1, HIGH);
  in_pcf8574.digitalWrite(P2, HIGH);
  in_pcf8574.digitalWrite(P3, HIGH);
  in_pcf8574.digitalWrite(P4, HIGH);
  in_pcf8574.digitalWrite(P5, HIGH);
  in_pcf8574.digitalWrite(P6, HIGH);
  in_pcf8574.digitalWrite(P7, HIGH);
  
  in_pcf8574.begin();

  inputs = in_pcf8574.digitalReadAll();

  (inputs.p0 == HIGH) ? out_pcf8574.digitalWrite(P0, HIGH) : out_pcf8574.digitalWrite(P0, LOW);
  (inputs.p1 == HIGH) ? out_pcf8574.digitalWrite(P1, HIGH) : out_pcf8574.digitalWrite(P1, LOW);
  (inputs.p2 == HIGH) ? out_pcf8574.digitalWrite(P2, HIGH) : out_pcf8574.digitalWrite(P2, LOW);
  (inputs.p3 == HIGH) ? out_pcf8574.digitalWrite(P3, HIGH) : out_pcf8574.digitalWrite(P3, LOW);
  (inputs.p4 == HIGH) ? out_pcf8574.digitalWrite(P4, HIGH) : out_pcf8574.digitalWrite(P4, LOW);
  (inputs.p5 == HIGH) ? out_pcf8574.digitalWrite(P5, HIGH) : out_pcf8574.digitalWrite(P5, LOW);
  (inputs.p6 == HIGH) ? out_pcf8574.digitalWrite(P6, HIGH) : out_pcf8574.digitalWrite(P6, LOW);
  (inputs.p7 == HIGH) ? out_pcf8574.digitalWrite(P7, HIGH) : out_pcf8574.digitalWrite(P7, LOW);


  attachInterrupt(digitalPinToInterrupt(2), interrupt_service_routine, FALLING);
}

void loop()
{  
  if (trigger_on)
  {
    Serial.println(F("Trigger."));
    trigger_on = false;
    inputs = in_pcf8574.digitalReadAll();
  }

  (inputs.p0 == HIGH) ? out_pcf8574.digitalWrite(P0, HIGH) : out_pcf8574.digitalWrite(P0, LOW);
  (inputs.p1 == HIGH) ? out_pcf8574.digitalWrite(P1, HIGH) : out_pcf8574.digitalWrite(P1, LOW);
  (inputs.p2 == HIGH) ? out_pcf8574.digitalWrite(P2, HIGH) : out_pcf8574.digitalWrite(P2, LOW);
  (inputs.p3 == HIGH) ? out_pcf8574.digitalWrite(P3, HIGH) : out_pcf8574.digitalWrite(P3, LOW);
  (inputs.p4 == HIGH) ? out_pcf8574.digitalWrite(P4, HIGH) : out_pcf8574.digitalWrite(P4, LOW);
  (inputs.p5 == HIGH) ? out_pcf8574.digitalWrite(P5, HIGH) : out_pcf8574.digitalWrite(P5, LOW);
  (inputs.p6 == HIGH) ? out_pcf8574.digitalWrite(P6, HIGH) : out_pcf8574.digitalWrite(P6, LOW);
  (inputs.p7 == HIGH) ? out_pcf8574.digitalWrite(P7, HIGH) : out_pcf8574.digitalWrite(P7, LOW);

  Serial.println(F("I'll just keep looping..."));
  delay(50);
}

void interrupt_service_routine()
{  
  if (millis() > prev_millis + 50)
  {
    trigger_on = true;
    prev_millis = millis();

    Serial.println("ISR - Interrupt Service Routine.");
  }
}
