#include "Arduino.h"
#include <Wire.h>

#define BIT(x) ((unsigned char) (1 << (x)))

#define OUTPUT_DEV_ADDR 0x20
#define INPUT_DEV_ADDR  0x21

#define NUM_OF_BYTES 1

#define INT_PIN 2

volatile boolean trigger_on = false;
unsigned long prev_millis = millis();

void setup()
{
	Serial.begin(9600);
	Wire.begin();

	Serial.print(F("INPUT PCF8574 device at address: "));
	Serial.println(INPUT_DEV_ADDR);

	Serial.print(F("OUTPUT PCF8574 device at address: "));
	Serial.println(OUTPUT_DEV_ADDR);

	Serial.print(F("Initializing input pins of PCF8574 @ "));
	Serial.println(INPUT_DEV_ADDR);

	byte init_byte = 1;
	for(int i = 0; i < 9; i++)
	{
		Wire.beginTransmission(INPUT_DEV_ADDR);
		Wire.write(init_byte);
		Wire.endTransmission();
		init_byte <<= 1;
	}

	Serial.println(F("The setup of the PCF8754 I2C expanders is complete..."));
	Serial.println(F("Reading the state of the inputs..."));

	byte val = 0;
	Wire.requestFrom(INPUT_DEV_ADDR, NUM_OF_BYTES);
	if (Wire.available())
	{
		val = Wire.read();

		Wire.beginTransmission(OUTPUT_DEV_ADDR);
		Wire.write(~val);
		Wire.endTransmission();
	}

	attachInterrupt(digitalPinToInterrupt(INT_PIN), input_change_isr, FALLING);
}

void loop()
{
	if (trigger_on)
	{
		Serial.println(F("TRIGGER - Inputs change..."));
		trigger_on = false;

		byte val = 0;
		Wire.requestFrom(INPUT_DEV_ADDR, NUM_OF_BYTES);
		if (Wire.available())
		{
			val = Wire.read();

			Wire.beginTransmission(OUTPUT_DEV_ADDR);
			Wire.write(~val);
			Wire.endTransmission();
		}
	}

	delay(50);
}

void input_change_isr()
{
  if (millis() > prev_millis + 50)
  {
    trigger_on = true;
    prev_millis = millis();

    Serial.println(F("ISR - Interrupt Service Routine..."));
  }
}
